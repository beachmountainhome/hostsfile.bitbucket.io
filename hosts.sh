now=`date +%Y%m%d-%H:%M:%S`
git fetch --all # geta ll
git reset --hard origin/master # force to ensure whitelist

rm -f hosts
cat hostslists.txt | while read line
do
    wget -O ./tmp "$line" # force to ensure 
    cat ./tmp >> hosts
done
rm -f ./tmp

awk '/^0.0.0.0 /' hosts > hosts.cleaned
awk '/^127.0.0.1 /' hosts >> hosts.cleaned
rm -f ./hosts
sed -i '/ 0.0.0.0/d' ./hosts.cleaned # 0 but not at start
sed -i '/ 127.0.0.1/d' ./hosts.cleaned # 127 but not at start
sed -i '/ localhost/d' ./hosts.cleaned # 127 but not at start


cat hosts.cleaned > hosts

rm -f ./hosts.cleaned

cat whitelist.txt | while read line
do
    echo "$line"
    sed -i "/${line}/d" ./hosts # -i edits hosts file directly, add space to be strict only 
done

# new
cat blacklist.txt >> ./hosts


sed -i 's/0.0.0.0/127.0.0.1/g' ./hosts # homogenious
grep '^127.0.0.1 ' ./hosts > ./hostsraw # security, to not have other IPs #enforce 127.0.0.1 


cat hosts.linux.header > hosts
cat hostsraw >> hosts


awk '!NF || !seen[$0]++' ./hosts > ./hostsnondup && mv ./hostsnondup ./hosts # remove duplicates
git add -A && git commit -m "hostsfile update $now"
git push --force

